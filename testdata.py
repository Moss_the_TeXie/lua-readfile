import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.integrate import odeint

# Parameter
picname ="Aizawa"
a = 0.95
b = 0.7
c = 0.6
d = 3.5
e = 0.25
f = 0.1

# basic equation
def calculate(u, t, a, b,c,d,e,f):
    x, y, z = u
    # Aizawa
    dxdt = (z-b)*x - d*y
    dydt = d*x + (z-b)*y
    dzdt = c + a*z - pow(z,3)/3 -(x*x + y*y)*(1 + e*z) + f*z*pow(x,3) 
    return(dxdt, dydt, dzdt)

# start values and initialization
y0 = -1.0, 0.9, 1.0
t = np.linspace(0, 40, 2000)
solution = odeint(calculate, y0, t, args = (a, b,c,d,e,f))
X, Y, Z = solution[:,0], solution[:,1], solution[:,2]

#####################################################
fig_a=plt.figure()
ax_a= fig_a.add_subplot(111,title="XY")
ax_a.axis("Off")
ax_a.plot(X, Y, "black")
fig_a.savefig(picname + "_XY.pdf", bbox_inches='tight')

fig_b=plt.figure()
ax_b= fig_b.add_subplot(111,title="XZ")
ax_b.axis("Off")
ax_b.plot(X, Z, "black")
fig_b.savefig(picname + "_XZ.pdf", bbox_inches='tight')

fig_c=plt.figure()
ax_c= fig_c.add_subplot(111,title="YZ")
ax_c.axis("Off")
ax_c.plot(Y, Z, "black")
fig_c.savefig(picname + "_YZ.pdf", bbox_inches='tight')

fig_d=plt.figure()
ax_d= fig_d.add_subplot(111,projection = "3d",title="3d_"+picname)
ax_d.set_xticklabels([])
ax_d.set_yticklabels([])
ax_d.set_zticklabels([])
ax_d.plot(X, Y, Z, "black")
fig_d.savefig(picname + "_3d.pdf", bbox_inches='tight')


fig2 = plt.figure()
ax2 = fig2.add_subplot(221,title="XY")
ax3 = fig2.add_subplot(222,title="XZ")
ax4 = fig2.add_subplot(223,title="YZ")
ax5 = fig2.add_subplot(224,projection = "3d",title="3d_"+picname)
ax2.axis("Off")
ax3.axis("Off")
ax4.axis("Off")
ax2.plot(X, Y, "black")
ax3.plot(X, Z, "black")
ax4.plot(Y, Z, "black")
ax5.set_xticklabels([])
ax5.set_yticklabels([])
ax5.set_zticklabels([])
ax5.plot(X, Y, Z, "black")
fig2.savefig(picname + "_4.pdf", bbox_inches='tight') 
plt.show()